import Zoom from './src/zoom'

Zoom.install = function (Vue) {
  Vue.component(Zoom.name, Zoom)
}
export default Zoom